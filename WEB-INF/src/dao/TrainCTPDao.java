package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dto.TrainCTP;
import utils.DS;

public class TrainCTPDao {
	public static boolean create(TrainCTP trainCTP) {
		try {
			PreparedStatement ps = DS.getConnection().prepareStatement("INSERT INTO train(prenom, nom) VALUES(?,?)");
			ps.setString(1, trainCTP.getFirstName());
			ps.setString(2, trainCTP.getLastName());
			if(ps.executeUpdate() > 0) {
				return true;
			}
		} catch(SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	public static TrainCTP find(int id) {
		try {
			PreparedStatement ps = DS.getConnection().prepareStatement("SELECT * FROM train WHERE id = ?");
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			if(!rs.next()) return null;
			return new TrainCTP(rs.getInt("id"), rs.getString("prenom"), rs.getString("nom"));
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static List<TrainCTP> findAll() {
		List<TrainCTP> res = new ArrayList<>();
		try {
			PreparedStatement ps = DS.getConnection().prepareStatement("SELECT * FROM train");
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				res.add(new TrainCTP(rs.getInt("id"), rs.getString("prenom"), rs.getString("nom")));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public static boolean update(TrainCTP trainCTP) {
		try {
			PreparedStatement ps = DS.getConnection().prepareStatement("UPDATE train SET prenom = ?, nom = ? WHERE id = ?");
			ps.setString(1, trainCTP.getFirstName());
			ps.setString(2, trainCTP.getLastName());
			ps.setInt(3, trainCTP.getId());
			if(ps.executeUpdate() > 0) {
				return true;
			}
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	public static boolean delete(int id) {
		try {
			PreparedStatement ps = DS.getConnection().prepareStatement("DELETE FROM train WHERE id = ?");
			ps.setInt(1, id);
			if(ps.executeUpdate() > 0) {
				return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	public static boolean delete(TrainCTP trainCTP) {
		return TrainCTPDao.delete(trainCTP.getId());
	}
}
