package utils;


import java.sql.*;

public class DS {
	private static DS _instance;
	private final Connection connection;

	private DS() throws Exception {
		Class.forName(Config.getProperty("driver"));
		String url = Config.getProperty("url");
		String username = Config.getProperty("username");
		String password = Config.getProperty("password");
		this.connection = DriverManager.getConnection(url, username, password);
	}

	public static Connection getConnection() {
		if(DS._instance == null) {
			try {
				DS._instance = new DS();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return DS._instance.connection;
	}

	public static void main(String[] args) {
		System.out.println(DS.getConnection());
	}
}
