package utils;


import java.util.Properties;

public class Config {
	public Properties config;
	public static Config _instance;

	public Config() throws Exception {
		this.config = new Properties();
		this.config.load(this.getClass().getResourceAsStream("/resources/config.prop"));
	}

	private Properties getProp() {
		return this.config;
	}

	private static Config getConfig() {
		if(Config._instance == null) {
			try {
				Config._instance = new Config();
			} catch (Exception e) {
				System.out.println("Couldnt load config file...");
			}
		}
		return Config._instance;
	}

	public static String getProperty(String name) {
		return Config.getConfig().getProp().getProperty(name);
	}
	
	public static void main(String[] args) {
		DS.getConnection();
	}
}
