package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.TrainCTPDao;
import dto.TrainCTP;

@WebServlet(
		name = "TrainDelete",
		description = "TrainDelete controller",
		urlPatterns = "/deleteTrain"
)
public class TrainDeleteController extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		try {
			int id = Integer.parseInt(req.getParameter("id"));
			TrainCTPDao.delete(id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		res.sendRedirect("trainlist");
		return;
	}
}
