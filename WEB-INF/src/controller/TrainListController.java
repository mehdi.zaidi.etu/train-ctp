package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.TrainCTPDao;
import dto.TrainCTP;

@WebServlet(
		name = "TrainList",
		description = "TrainList controller",
		urlPatterns = "/trainlist"
)
public class TrainListController extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		List<TrainCTP> allTrains = TrainCTPDao.findAll();
		req.setAttribute("trains", allTrains);
		req.getRequestDispatcher("/views/alltrains.jsp").forward(req, res);
		return;
	}
}
