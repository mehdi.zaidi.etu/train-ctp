package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.TrainCTPDao;
import dto.TrainCTP;

@WebServlet(
		name = "TrainEdit",
		description = "TrainEdit controller",
		urlPatterns = "/editTrain"
)
public class TrainEditController extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		try {
			int id = Integer.parseInt(req.getParameter("id"));
			TrainCTP train = TrainCTPDao.find(id);
			if(train != null) {
				req.setAttribute("train", train);
				req.getRequestDispatcher("/views/editTrain.jsp").forward(req, res);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		res.sendRedirect("trainlist");
		return;
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		try {
			int id =  Integer.parseInt(req.getParameter("id"));
			String firstName = req.getParameter("train-firstName");
			String lastName = req.getParameter("train-lastName");
			TrainCTPDao.update(new TrainCTP(id, firstName, lastName));
		} catch (Exception e) {
			e.printStackTrace();
		}
		res.sendRedirect("trainlist");
		return;
	}
}
