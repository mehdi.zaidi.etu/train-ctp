package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.TrainCTPDao;
import dto.TrainCTP;

@WebServlet(
		name = "TrainCreate",
		description = "TrainCreate controller",
		urlPatterns = "/createTrain"
)
public class TrainCreateController extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		req.getRequestDispatcher("/views/createTrain.jsp").forward(req, res);
		return;
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		try {
			String firstName = req.getParameter("train-firstName");
			String lastName = req.getParameter("train-lastName");
			TrainCTPDao.create(new TrainCTP(0, firstName, lastName));
		} catch (Exception e) {
			e.printStackTrace();
		}
		res.sendRedirect("trainlist");
		return;
	}
}
