DROP TABLE IF EXISTS train;

CREATE TABLE train(
    id SERIAL,
    nom TEXT,
    prenom TEXT
);

INSERT INTO train(nom,prenom) VALUES
    ('Kelly', 'LeTrain'),
    ('Dunbar', 'LeTrain'),
    ('Emery', 'LeTrain'),
    ('Harry', 'LeTrain'),
    ('Hermione', 'LeTrain');