<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    <title>All Trains</title>
</head>
<body>
    <%@ page pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" %>
    <%@ page import="dto.TrainCTP, java.util.List" %>
    <% List<TrainCTP> allTrains = (List<TrainCTP>) request.getAttribute("trains"); %>
    <div class="container">
        <h1 class="my-3">Liste des trains</h1>
        <table class="table text-center">
            <tr>
                <th>id</th>
                <th>nom</th>
                <th>prénom</th>
                <th>Actions</th>
            </tr>
            <% for(TrainCTP train : allTrains) { %>
                <tr>
                    <td><%= train.getId() %></td>
                    <td><%= train.getFirstName() %></td>
                    <td><%= train.getLastName() %></td>
                    <td><a href="/train/deleteTrain?id=<%= train.getId() %>" class="btn btn-sm btn-danger me-1">Supprimer</a><a href="/train/editTrain?id=<%= train.getId() %>" class="btn btn-sm btn-primary">Editer</a></td>
                </tr>
            <% } %>
        </table>
        <a href="/train/createTrain" class="btn btn-success">Ajouter un train</a>
    </div>
</body>
</html>