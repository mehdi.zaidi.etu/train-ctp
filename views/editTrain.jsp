<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    <title>All Trains</title>
</head>
<body>
    <%@ page pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" %>
    <%@ page import="dto.TrainCTP" %>
    <% TrainCTP train = (TrainCTP) request.getAttribute("train"); %>
    <div class="container">
        <form method="POST">
            <input type="number" class="form-control my-2" value="<%= train.getId() %>" disabled />
            <input type="text" class="form-control my-2" name="train-firstName" value="<%= train.getFirstName() %>" required />
            <input type="text" class="form-control my-2" name="train-lastName" value="<%= train.getLastName() %>" required />
            <input type="submit" class="btn btn-success my-2" value="Appliquer les modifications" />
        </form>
    </div>
</body>
</html>