# Entrainement CTP Archi Logicielle 

## Auteur : [Mehdi Zaidi](https://hidoyat.fr)
## Date : 28/02/2023

### Petit projet java avec un exemple de MVC avec un DAO
- Model = Les fichiers dans ./WEB-INF/src/dao
- Contrôleur = Les servlets dans ./WEB-INF/src/controller
- Vues = Les fichiers jsp dans ./views